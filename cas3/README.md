# Descriptif du cas

Code terraform modularisé où le module hérite implicitement du provider par défaut défini implicitement.

# Remarque

Ici on peut voir que la ressource est attachée au provider par défaut défini et hérité implicitement:
```
"provider": "provider.aws"
```