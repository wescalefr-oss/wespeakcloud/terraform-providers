# Descriptif du cas

Code terraform modularisé où le module définit explicitement un provider alias vide.

# Remarque

Ici on peut voir que la ressource est attachée au provider alias défini dans le module:
```
"provider": "module.example.provider.aws.test"
```