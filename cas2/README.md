# Descriptif du cas

Simple code terraform non modularisé utilisant un provider avec alias vide déclaré explicitement.

# Remarque

Ici on peut voir que la ressource est attachée au provider alias déclaré explicitement:
```
"provider": "provider.aws.test"
```