# Descriptif du cas

Code terraform modularisé où la racine définit explicitement un provider paramétré avec un alias et une ressource du module appelle ce nom d'alias bien que le module ne l'ait pas déclaré.

# Remarque

Ici l'exécution est impossible. Terraform remonte l'erreur suivante: ![capture](./capture.png)