# Description

Ce projet présente plusieurs cas de démonstration des relations des _resources/data_ dans du code Terraform et leurs providers associés selon l'endroit de déclaration et les héritages déclarés implicitement ou explicitement. Chaque dossier est fourni avec un terraform state créé par le code.

L'ensemble des exécutions a été effectué avec la version `0.12.25` de Terraform et devrait normalement expliciter le comportement de la branche 0.12.x du logiciel.

# Index des cas de démonstration

- [cas1](./cas1): Simple code terraform non modularisé utilisant le provider par défaut déclaré implicitement
- [cas2](./cas2): Simple code terraform non modularisé utilisant un provider avec alias vide déclaré explicitement
- [cas3](./cas3): Code terraform modularisé où le module hérite implicitement du provider par défaut défini implicitement
- [cas4](./cas4): Code terraform modularisé où le module redéfinit explicitement le provider par défaut vide
- [cas5](./cas5): Code terraform modularisé où le module définit explicitement un provider alias vide
- [cas6](./cas6): Code terraform modularisé où la racine et le module définissent explicitement un provider vide avec le même alias sans surcharge explicite
- [cas7](./cas7): Code terraform modularisé où la racine et le module définissent explicitement un provider vide avec le même alias avec surcharge explicite
- [cas8](./cas8): Code terraform modularisé où la racine et le module définissent explicitement un provider paramétré avec le même alias avec surcharge explicite
- [cas9](./cas9): Code terraform modularisé où la racine définit explicitement un provider paramétré avec un alias et une ressource du module appelle ce nom d'alias bien que le module ne l'ait pas déclaré
- [cas10](./cas10): Code terraform modularisé où la racine et le module définissent explicitement un provider paramétré avec des alias différents avec surcharge explicite