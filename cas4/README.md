# Descriptif du cas

Code terraform modularisé où le module redéfinit explicitement le provider par défaut vide.

# Remarque

Ici on peut voir que la ressource est attachée au provider défini dans le module:
```
"provider": "module.example.provider.aws"
```