# Descriptif du cas

Code terraform modularisé où la racine et le module définissent explicitement un provider vide avec le même alias sans surcharge explicite.

# Remarque

Ici on peut voir que la ressource est attachée au provider alias défini dans le module:
```
"provider": "module.example.provider.aws.test"
```