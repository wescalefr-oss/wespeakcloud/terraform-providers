# Descriptif du cas

Code terraform modularisé où la racine et le module définissent explicitement un provider paramétré avec des alias différents avec surcharge explicite.

# Remarque

Ici on peut voir que la ressource est attachée au provider alias défini dans la racine (le nom de l'alias défini dans le module n'est présent nulle part):
```
"provider": "provider.aws.test"
```