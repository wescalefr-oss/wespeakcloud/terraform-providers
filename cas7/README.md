# Descriptif du cas

Code terraform modularisé où la racine et le module définissent explicitement un provider vide avec le même alias avec surcharge explicite.

# Remarque

Ici on peut voir que la ressource est attachée au provider alias défini dans la racine qui surcharge complètement le provider du module:
```
"provider": "provider.aws.test"
```