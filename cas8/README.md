# Descriptif du cas

Code terraform modularisé où la racine et le module définissent explicitement un provider paramétré avec le même alias avec surcharge explicite.

# Remarque

Ici on peut voir que la ressource est attachée au provider alias défini dans le module même si on a explicitement voulu le surcharger (confirmé par la région renvoyée dans la datasource):
```
"provider": "module.example.provider.aws.test"
```