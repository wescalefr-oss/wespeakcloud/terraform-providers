# Descriptif du cas

Simple code terraform non modularisé utilisant le provider par défaut déclaré implicitement.

# Remarque

Ici on peut voir que la ressource est attachée au provider par défaut déclaré implicitement:
```
"provider": "provider.aws"
```